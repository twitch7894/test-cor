import React, { useState } from "react";
import Directions from "../utils";
import Modal from "./modal";

function Items({ data, word }) {
  const [isShowing, setIsShowing] = useState(false);
  const [num, setNum] = useState(0);
  const [info, setInfo] = useState([]);
  
  const Alphabet_Soup = () => {
    let count = 0;

    for (let y = 0; y < data.length; y++) {
      for (let x = 0; x < data[y].length; x++) {
        if (data[y][x] === word[0]) {
          count += Directions.horizontal(y, x, data, word);
          count += Directions.verticalUp(y, x, data, word);
          count += Directions.diagonalRightUp(y, x, data, word);
          count += Directions.diagonalLeft(y, x, data, word);
          count += Directions.horizontalBack(y, x, data, word);
          count += Directions.verticalDown(y, x, data, word);
          count += Directions.diagonalLeftDown(y, x, data, word);
          count += Directions.diagonal(y, x, data, word);
        }
      }
    }
    setIsShowing(true);
    setNum(count);
    setInfo(data);
  };
  return (
    <div className="container">
      {data.map((item, k) => (
        <div key={k} className="row">
          {item.map((e, i) => (
            <span key={i} className="Soup">
              {e}
            </span>
          ))}
        </div>
      ))}
      <button className="button" onClick={Alphabet_Soup}>
        Count
      </button>
      <Modal
        isShowing={isShowing}
        hide={setIsShowing}
        count={num}
        info={info}
      />
    </div>
  );
}

export default Items;
