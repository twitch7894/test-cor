import React from "react";
import ReactDOM from "react-dom";

const Modal = ({ isShowing, hide, count, info }) =>
  isShowing
    ? ReactDOM.createPortal(
        <React.Fragment>
          <div className="modal-overlay" />
          <div
            className="modal-wrapper"
            aria-modal
            aria-hidden
            tabIndex={-1}
            role="dialog"
          >
            <div className="modal">
              <div className="modal-header">
                <button
                  type="button"
                  className="modal-close-button"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={() => hide(false)}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="container">
                {info.map((item, k) => {
                  return (
                    <div key={k} className="Modal-Row">
                      {item.map((e, i) => {
                        return (
                          <span key={i} className={"Soup"}>
                            {e}
                          </span>
                        );
                      })}
                    </div>
                  );
                })}
              </div>
              <h1 className="title">
                Result <strong className="Result-Modal">{count}</strong>
              </h1>
            </div>
          </div>
        </React.Fragment>,
        document.body
      )
    : null;

export default Modal;
