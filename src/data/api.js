import { useReducer } from "react";
import axios from "axios";
import reducer, { initialState } from "../reducer";
import { fetching, success, error } from "../actions";

const useApiRequest = (endpoint, { verb = "get" } = {}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const makeRequest = async () => {
    dispatch(fetching());
    try {
      const response = await axios[verb](endpoint);
      console.log("test_response", response.data);
      dispatch(success(response));
    } catch (e) {
      dispatch(error(e));
    }
  };

  return [state, makeRequest];
};

export default useApiRequest;
