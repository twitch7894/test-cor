const prefix = "TEST_COR/";

export const FETCHING = `${prefix}FETCHING`;
export const SUCCESS = `${prefix}SUCCESS`;
export const ERROR = `${prefix}ERROR`;
