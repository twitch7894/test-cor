/**
 * ENDPOINTS
 */

// ENDPOINT TEST COR
const URL = "https://my-json-server.typicode.com/javier526/test-json/db";

export const ENDPOINTS = {
  COR: `${URL}`
};
