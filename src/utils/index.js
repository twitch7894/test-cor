/**
 * Directions
 */

const Directions = {
  horizontal: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y][x + i] !== undefined &&
        direction[y][x + i + 1] !== undefined
      ) {
        if (
          direction[y][x + i] === word[i] &&
          direction[y][x + i + 1] === word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  },
  verticalUp: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y + i] !== undefined &&
        direction[y + i + 1] !== undefined
      ) {
        if (
          direction[y + i][x] === word[i] &&
          direction[y + i + 1][x] === word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  },
  diagonalRightUp: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y + i] !== undefined &&
        direction[y + i + 1] !== undefined
      ) {
        if (
          direction[y + i][x + i] == word[i] &&
          direction[y + i + 1][x + i + 1] == word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  },
  diagonalLeft: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y + i] !== undefined &&
        direction[y + i + 1] !== undefined
      ) {
        if (
          direction[y + i][x - i] === word[i] &&
          direction[y + i + 1][x - i - 1] === word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  },
  horizontalBack: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y][x - i] !== undefined &&
        direction[y][x - i - 1] !== undefined
      ) {
        if (
          direction[y][x - i] === word[i] &&
          direction[y][x - i - 1] === word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  },
  diagonalLeftDown: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y - i] !== undefined &&
        direction[y - i - 1] !== undefined
      ) {
        if (
          direction[y - i][x - i] == word[i] &&
          direction[y - i - 1][x - i - 1] == word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  },
  verticalDown: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y - i] !== undefined &&
        direction[y - i - 1] !== undefined
      ) {
        if (
          direction[y - i][x] == word[i] &&
          direction[y - i - 1][x] === word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  },
  diagonal: function(y, x, direction, word) {
    for (let i = 1; i < word.length; i++) {
      if (
        direction[y - i] !== undefined &&
        direction[y - i - 1] !== undefined
      ) {
        if (
          direction[y - i][x + i] == word[i] &&
          direction[y - i - 1][x + i + 1] == word[i + 1]
        ) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  }
};

export default Directions;
