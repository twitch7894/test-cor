import React, { useState } from "react";
import useApiRequest from "./data/api";
import { SUCCESS, ERROR, FETCHING } from "./constants";
import Items from "./components/items";
import { ENDPOINTS } from "./config/endPoints";

function ApiRequest() {
  const [letter, setLetter] = useState("");
  const [{ status, response }, makeRequest] = useApiRequest(ENDPOINTS.COR, {
    verb: "get"
  });
  const style = letter.length > 2 ? "button" : "button-disable";
  const valid = letter.length > 2 ? false : true;
  console.log("status", status);
  return (
    <React.Fragment>
      {status === FETCHING ? (
        <h1 className="loading">Loading...</h1>
      ) : (
        <div className="start">
          {status === SUCCESS ? null : (
            <h1 className="title">Welcome to Alphabet Soup</h1>
          )}
          <h3 className="title">
            Enter your search, minimum <strong className="Min-Letter">3</strong>{" "}
            letters
          </h3>
          <div className="container-button">
            <input
              type="string"
              value={letter}
              onChange={e => setLetter(e.target.value.toUpperCase())}
            />
            <button className={style} onClick={makeRequest} disabled={valid}>
              Search
            </button>
          </div>
        </div>
      )}
      {status === SUCCESS ? (
        <div className="start">
          {response.data.resources.map((data, index) => (
            <Items key={index} data={data} word={letter} />
          ))}
        </div>
      ) : null}
      {status === ERROR ? <h1 className="error">Not Found Error 404</h1> : null}
    </React.Fragment>
  );
};

export default ApiRequest;
