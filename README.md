## Quick Overview

```sh
git clone https://gitlab.com/twitch7894/test-cor
cd test-cor
npm install o yarn
```
Link: [https://zealous-fermat-2714b0.netlify.com/](https://zealous-fermat-2714b0.netlify.com/)<br>
Then open [http://localhost:8080/](http://localhost:8080/) to see your app.<br>
when you’re ready to deploy to development, bundle with `yarn dev`.
when you’re ready to deploy to production, bundle with `yarn prod`.